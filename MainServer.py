import socket

if __name__ == '__main__':
    tcp_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_address = ('localhost', 51234)
    tcp_socket.bind(server_address)
    tcp_socket.listen()

    while True:
        print('Waiting for connection')
        connection, client = tcp_socket.accept()

        try:
            print(f'Client connected: {client}')
            while True:
                data = connection.recv(32)
                print(f'Received data: {data}')
                if not data:
                    break
        finally:
            connection.close()