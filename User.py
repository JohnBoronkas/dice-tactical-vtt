from enum import Enum

class UserType(Enum):
    GameMaster = 10
    Player = 20

class User:
    def __init__(self, name, user_type):
        self._name = name
        self._user_type = user_type

    def get_name(self):
        return self._name

    def get_user_type(self):
        return self._user_type

    def __str__(self):
        return f'{self._name} ({self._user_type})'
