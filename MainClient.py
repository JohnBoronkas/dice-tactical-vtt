from User import User, UserType

import socket

if __name__ == '__main__':
    user = User('John', UserType.Player)
    tcp_socket = socket.create_connection(('localhost', 51234))
    
    try:
        tcp_socket.sendall(f'join:{user.get_name()}:{user.get_user_type()}'.encode())
    except:
        print('Closing client tcp socket')
        tcp_socket.close()

    while True:
        pass